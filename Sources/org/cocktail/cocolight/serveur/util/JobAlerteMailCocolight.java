package org.cocktail.cocolight.serveur.util;

import java.util.Map;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.AvenantEvenement;
import org.cocktail.fwkcktlevenement.common.util.FwkCktlEvenementUtil;
import org.cocktail.fwkcktlevenement.common.util.quartz.MailComposer;
import org.cocktail.fwkcktlevenement.serveur.quartz.job.impl.JobAlerteMail;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOApplication;

import er.extensions.eof.ERXEC;

public class JobAlerteMailCocolight extends JobAlerteMail {
	
	@Override
	protected void initializeMailContent() {
	    CktlConfig conf = ((CktlWebApplication)WOApplication.application()).config();
	    
		String grhumHostMail = conf.stringForKey("GRHUM_HOST_MAIL");
		String mailSender = conf.stringForKey("EVT_MAIL_SENDER");
		String mailReplyTo = conf.stringForKey("EVT_MAIL_REPLY_TO");
		
		MailComposer mailComposer = MailComposer.newInstance(grhumHostMail,	mailSender,	mailReplyTo); 
		
		String adressesMails = 
			getAdressesMailPersonnesConcernees().componentsJoinedByString(FwkCktlEvenementUtil.RECIPIENTS_SEPARATOR);

		mailComposer.setAdressesMailDestinataires(adressesMails);
		mailComposer.setInfosPersonnesInjoignables(getPersonnesConcerneesSansAdresseMail());
		
		AvenantEvenement avenantEvenement = AvenantEvenement.fetch(ERXEC.newEditingContext(), AvenantEvenement.EVENEMENT.eq(getEvenement()));
		String infosEvenement = 
				"<b>Convention " + avenantEvenement.avenant().contrat().numeroContrat() + "</b><br/><br/>" +
				"<b>Objet:</b> " + avenantEvenement.avenant().contrat().conObjet() + "<br/><br/>" + 
				getEvenement().getHtmlDescriptionPourMail();
		mailComposer.setSubject("[EVENEMENT] Alerte automatique - Convention " + avenantEvenement.avenant().contrat().numeroContrat());
		mailComposer.setInfosEvenement(infosEvenement);
//		mailComposer.setInfosTache(getTache().getHtmlDescriptionPourMail()); pas interessant pour le commun des mortels!!
		mailComposer.setInfosDelaiPrevenance(getTache().triggerOffsetInMinutesToString());
		mailComposer.setInfosProchainMail(getMailNextFireTime());
		mailComposer.setInfosMisfired(isMisfired());
		Map map = mailComposer.toPropertiesMap();

		getJobexecutioncontext().getMergedJobDataMap().putAll(map);
	}

}
