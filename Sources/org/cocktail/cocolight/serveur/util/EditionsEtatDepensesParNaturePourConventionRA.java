package org.cocktail.cocolight.serveur.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.cocolight.serveur.CocolightApplicationUser;
import org.cocktail.cocowork.server.CocoworkApplicationUser;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderConvention;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeOrgan;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;

public class EditionsEtatDepensesParNaturePourConventionRA {
  static final public Logger LOG = Logger.getLogger(EditionsGeneralitesConvention.class);
  
  /**
   * Point d'acces pour la creation de l'edition des generalites des
   * conventions.
   */
  static public CktlAbstractReporter editionConventionsGeneralites(Integer utilisateurPersId, Integer exercice, IJrxmlReportListener listener) {
      String masterJasperFileName = "EtatDepensesParNaturePourConventionRA.jasper";
      // point d'entree dans l'arborescence decrite dans le fichier de donnees
      // XML
      String recordPath = "/Etat/Ligne";
      JrxmlReporterWithXmlDataSource jr = null;
      try {
          // generation et ecriture du fichier XML produit :
          StringWriter xmlString = createXmlEditionEtatDepensesParNaturePourConventionRA(utilisateurPersId, exercice);
          // Affichage du xml en mode debug uniquement
          LOG.debug(xmlString);
          // flux fichier associe au fichier XML :
          InputStream xmlFileStream = new ByteArrayInputStream(xmlString.toString().getBytes());
          // parametres $P{...} passes au report principal : par ex titres,
          // chemins vers les sous reports, etc.
          Map<String, Object> parameters = new HashMap<String, Object>();
          jr = new JrxmlReporterWithXmlDataSource();
          jr.printWithThread("Etat des dépenses par natures", xmlFileStream, recordPath, pathForReportGeneralites(masterJasperFileName), parameters, CktlAbstractReporter.EXPORT_FORMAT_XLS, true, listener);
      }
      catch (Throwable e) {
          LOG.error("Une erreur s'est produite durant l'edition de l'état des dépenses par nature", e);
      }
      // retourne le resultat au client :
      return jr;
  }

  private static String pathForReportGeneralites(String reportName) {
      ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
      URL url = rsm.pathURLForResourceNamed("Reports/EtatDepensesParNaturePourConventionRA/" + reportName, "app", null);
      return url.getFile();
  }

  /**
   * Interroge la base pour generer le fichier de donnees XML qui sera utilise
   * pour "nourrir" l'edition.
   * (Utilisation de la bibliotheque du CRI de La Rochelle)
   */
  static public StringWriter createXmlEditionEtatDepensesParNaturePourConventionRA(Integer utilisateurPersId, Integer exercice) throws Exception {

    EOEditingContext edc = ERXEC.newEditingContext();
    
    NSArray<NSDictionary<String, Object>> lignes = EOUtilities.rawRowsForSQL(edc, "FwkCktlPersonne", requete(utilisateurPersId, exercice, edc), null);
    
    StringWriter sw = new StringWriter();
    CktlXMLWriter w = new CktlXMLWriter(sw);
    w.startDocument();
    w.startElement("Etat");
    
      for(NSDictionary<String, Object> ligne : lignes) {
        
        w.startElement("Ligne");
        
        w.writeElement("SousCr", (String) ligne.valueForKey("ORG_SOUSCR"));
        w.writeElement("Nature", (String) ligne.valueForKey("PCO_NUM"));
        w.writeElement("Libelle", (String) ligne.valueForKey("PCO_LIBELLE"));
        w.writeElement("BudgetPos", "" + (Double) ligne.valueForKey("POSITIONNE"));
        w.writeElement("Engagement", "" + (Double) ligne.valueForKey("MONTANT_ENG"));
        w.writeElement("Liquidation", "" + (Double) ligne.valueForKey("MONTANT_FACT"));
        w.writeElement("TotalDep", "" + (Double) ligne.valueForKey("MONTANT_TOTAL"));
        w.writeElement("DisponibleTheorique", "" + (Double) ligne.valueForKey("DISPONIBLE"));

        w.endElement();
        
      }
    
    w.endElement();
    w.endDocument();
    w.close();
    return sw;
  }
  
  
  static public String requete(Integer utilisateurPersId, Integer exercice, EOEditingContext edc) {
    
    
    // Récupération du login
    String login = EOCompte.fetchFirstByQualifier(edc, EOCompte.PERS_ID.eq(utilisateurPersId)).cptLogin();
    
    // Recuperation des sous crs type convention RA
    EOQualifier organQualifier = ERXQ.and(
                                    ERXQ.equals(ERXQ.keyPath(EOOrgan.TYPE_ORGAN_KEY, EOTypeOrgan.TYOR_LIBELLE_KEY), EOTypeOrgan.TYPE_CONVENTION_RA),
                                    EOOrgan.QUAL_NIVEAU_SOUSCR
                                    );
    
    
    NSArray<EOOrgan> organs = EOOrgan.fetchAll(edc, organQualifier);
    NSArray<Integer> orgIds = new NSMutableArray<Integer>();
    
    for(EOOrgan organ : organs) {
      NSDictionary<String, Object> primaryKey = EOUtilities.primaryKeyForObject(edc, organ);
      orgIds.add((Integer) primaryKey.valueForKey(EOOrgan.ORG_ID_KEY));
    }
    
    String orgIdsString = orgIds.componentsJoinedByString(",");
    
    
    // Recuperation des contrat sur lesquels l'utilisateur a les droits
    
    CocoworkApplicationUser applicationUser = new CocoworkApplicationUser(edc, utilisateurPersId);
    
    NSMutableDictionary<String, Object> bindings = new NSMutableDictionary<String, Object>();
    bindings.setObjectForKey(applicationUser.getUtilisateur().utlOrdre(), "utlOrdre");
    bindings.setObjectForKey("CONV", "codeClassification");
    bindings.setObjectForKey(applicationUser.hasDroitSuperAdmin(), "admin");
    bindings.setObjectForKey(applicationUser.hasDroitConsultationTousLesContratsEtAvenants(), "tous");
    NSArray<NSDictionary<Object, Object>> resultats = FinderConvention.getRawRowConventionForUser(edc, bindings);
    NSArray<Integer> conOrdres = (NSArray<Integer>) resultats.valueForKey("CONORDRE");
    String conOrdresString = conOrdres.componentsJoinedByString(",");
    
    // Generation de la requete
    
    StringWriter sw  = new StringWriter();
    sw.write("select");
    sw.write("  org_ub,");
    sw.write("  org_cr,");
    sw.write("  org_souscr,");
    sw.write("  pco_num,");
    sw.write("  pco_libelle,");
    sw.write("  sum(montant_eng) montant_eng,");
    sw.write("  sum(montant_fact) montant_fact,");
    sw.write("  sum(montant_eng + montant_fact) as montant_total,");
    sw.write("  sum(positionne) positionne,");
    sw.write("  sum (positionne - montant_eng - montant_fact) as disponible");
    sw.write(" from (");
    sw.write("  select");
    sw.write("    eb.org_id,");
    sw.write("    vo.org_lib,");
    sw.write("    vo.org_ub,");
    sw.write("    vo.org_cr,");
    sw.write("    vo.org_souscr,");
    sw.write("    eb.eng_id,");
    sw.write("    vpc.pco_num pco_num,");
    sw.write("    vpc.pco_libelle,");
    sw.write("    epco_montant_budgetaire_reste montant_eng,");
    sw.write("    0 montant_fact,");
    sw.write("    0 POSITIONNE");
    sw.write("  from jefy_admin.organ vo");
    sw.write("  inner join jefy_depense.engage_budget eb on eb.org_id = vo.org_id");
    sw.write("  inner join jefy_depense.engage_ctrl_planco ecp on eb.eng_id = ecp.eng_id");
    sw.write("  inner join maracuja.plan_comptable vpc on ecp.pco_num = vpc.pco_num");
    sw.write("  inner join jefy_admin.utilisateur_organ uo on uo.org_id = eb.org_id");
    sw.write("  inner join jefy_admin.utilisateur u on u.utl_ordre = uo.utl_ordre");
    sw.write("  inner join grhum.compte co on co.pers_id = u.pers_id");
    sw.write("  where");
    sw.write("    vo.org_id in (" + orgIdsString + ") and");
    sw.write("    eb.exe_ordre = " + exercice + " and");
    sw.write("    vpc.pco_nature like 'D' and");
    //sw.write("    -- vpc.pco_num like $P{CA_IMPUT} and");
    sw.write("    co.cpt_login = '" + login + "'");
    sw.write("  union all");
    sw.write("  select");
    sw.write("    eb.org_id,");
    sw.write("    vo.org_lib,");
    sw.write("    vo.org_ub,");
    sw.write("    vo.org_cr,");
    sw.write("    vo.org_souscr,");
    sw.write("    eb.eng_id,");
    sw.write("    vpc.pco_num pco_num,");
    sw.write("    vpc.pco_libelle,");
    sw.write("    0 montant_eng,");
    sw.write("    dpco_montant_budgetaire montant_fact,");
    sw.write("    0 POSITIONNE");
    sw.write("  from jefy_admin.organ vo");
    sw.write("  inner join jefy_depense.engage_budget eb on eb.org_id = vo.org_id");
    sw.write("  inner join jefy_depense.depense_budget db on eb.eng_id = db.eng_id");
    sw.write("  inner join jefy_depense.depense_ctrl_planco dcp on db.dep_id = dcp.dep_id");
    sw.write("  inner join maracuja.plan_comptable vpc on dcp.pco_num= vpc.pco_num");
    sw.write("  inner join jefy_admin.utilisateur_organ uo on uo.org_id = eb.org_id");
    sw.write("  inner join jefy_admin.utilisateur u on u.utl_ordre = uo.utl_ordre");
    sw.write("  inner join grhum.compte co on co.pers_id = u.pers_id");
    sw.write("    where ");
    sw.write("    vo.org_id in (" + orgIdsString + ") and");
    sw.write("    eb.exe_ordre = " + exercice + " and     ");
    sw.write("    vpc.pco_nature like 'D' and");
    //sw.write("    -- vpc.pco_num like $P{CA_IMPUT} and          ");
    sw.write("    co.cpt_login = '" + login + "'");
    sw.write("  union all");
    sw.write("  SELECT ");
    sw.write("    CRA.ORG_ID,");
    sw.write("    CRA.ORG_LIB,");
    sw.write("    CRA.ORG_UB,");
    sw.write("    CRA.ORG_CR,");
    sw.write("    CRA.ORG_SOUSCR,");
    sw.write("    0 eng_id,");
    sw.write("    SCP.PCO_NUM,");
    sw.write("    vpc.pco_libelle, ");
    sw.write("    0 montant_eng,");
    sw.write("    0 montant_fact,");
    sw.write("    SCP.POSITIONNE ");
    sw.write("  FROM ");
    sw.write("    ACCORDS.CONVENTION_RA CRA");
    sw.write("    inner join ACCORDS.V_SUIVI_CREDITS_POSITIONNES SCP on CRA.CON_ORDRE = SCP.CON_ORDRE");
    sw.write("    inner join maracuja.plan_comptable vpc on vpc.pco_num = SCP.pco_num");
    sw.write("  WHERE ");
    sw.write("    cra.org_id in (" + orgIdsString + ") and");
    sw.write("    SCP.exe_ordre = " + exercice + " and");
    sw.write("    cra.con_ordre in (" + conOrdresString + ")");
    sw.write(")");
    sw.write(" group by org_ub,org_cr,org_souscr,pco_num,pco_libelle");
    sw.write(" order by org_ub,org_cr,org_souscr,pco_num");
    return sw.toString();
  }
  
  /** REQUETE ORIGINALE 
   
select 
  org_ub,
  org_cr,
  org_souscr,
  pco_num,
  pco_libelle,
  sum(montant_eng) montant_eng,
  sum(montant_fact) montant_fact,
  sum(montant_eng + montant_fact) as montant_total,
  sum(positionne) positionne,
  sum (positionne - montant_eng - montant_fact) as disponible
from (
  select
    eb.org_id,
    vo.org_lib,
    vo.org_ub,
    vo.org_cr,
    vo.org_souscr,
    eb.eng_id,
    vpc.pco_num pco_num,
    vpc.pco_libelle,
    epco_montant_budgetaire_reste montant_eng,
    0 montant_fact,
    0 POSITIONNE
  from jefy_admin.organ vo
  inner join jefy_depense.engage_budget eb on eb.org_id = vo.org_id
  inner join jefy_depense.engage_ctrl_planco ecp on eb.eng_id = ecp.eng_id
  inner join maracuja.plan_comptable vpc on ecp.pco_num = vpc.pco_num
  inner join jefy_admin.utilisateur_organ uo on uo.org_id = eb.org_id
  inner join jefy_admin.utilisateur u on u.utl_ordre = uo.utl_ordre
  inner join grhum.compte co on co.pers_id = u.pers_id
  where
    vo.org_id in () and
    eb.exe_ordre = $P{CA_EXER} and
    vpc.pco_nature like 'D' and
    -- vpc.pco_num like $P{CA_IMPUT} and
    co.cpt_login = $P{CA_OPENREPORTS_USER_NAME}
  union all
  select
    eb.org_id,
    vo.org_lib,
    vo.org_ub,
    vo.org_cr,
    vo.org_souscr,
    eb.eng_id,
    vpc.pco_num pco_num,
    vpc.pco_libelle,
    0 montant_eng,
    dpco_montant_budgetaire montant_fact,
    0 POSITIONNE
  from jefy_admin.organ vo
  inner join jefy_depense.engage_budget eb on eb.org_id = vo.org_id
  inner join jefy_depense.depense_budget db on eb.eng_id = db.eng_id
  inner join jefy_depense.depense_ctrl_planco dcp on db.dep_id = dcp.dep_id
  inner join maracuja.plan_comptable vpc on dcp.pco_num= vpc.pco_num
  inner join jefy_admin.utilisateur_organ uo on uo.org_id = eb.org_id
  inner join jefy_admin.utilisateur u on u.utl_ordre = uo.utl_ordre
  inner join grhum.compte co on co.pers_id = u.pers_id
    where 
    vo.org_id in () and
    eb.exe_ordre = $P{CA_EXER} and     
    vpc.pco_nature like 'D' and
    -- vpc.pco_num like $P{CA_IMPUT} and          
    co.cpt_login = $P{CA_OPENREPORTS_USER_NAME}
  union all
  SELECT 
    CRA.ORG_ID,
    CRA.ORG_LIB,
    CRA.ORG_UB,
    CRA.ORG_CR,
    CRA.ORG_SOUSCR,
    0 eng_id,
    SCP.PCO_NUM,
    vpc.pco_libelle, 
    0 montant_eng,
    0 montant_fact,
    SCP.POSITIONNE 
  FROM 
    ACCORDS.CONVENTION_RA CRA
    inner join ACCORDS.V_SUIVI_CREDITS_POSITIONNES SCP on CRA.CON_ORDRE = SCP.CON_ORDRE
    inner join maracuja.plan_comptable vpc on vpc.pco_num = SCP.pco_num
  WHERE 
    vo.org_id in () and
    SCP.exe_ordre=$P{CA_EXER} and
    cra.con_ordre in ()
)
group by org_ub,org_cr,org_souscr,pco_num,pco_libelle                                             
order by org_ub,org_cr,org_souscr,pco_num
   
   
   */
  
  
  
}
