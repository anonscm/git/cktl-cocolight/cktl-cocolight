/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.cocolight.serveur.components;

import java.util.Date;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.service.recherche.DisciplineBean;
import org.cocktail.cocowork.server.metier.convention.service.recherche.Filtre;
import org.cocktail.cocowork.server.metier.convention.service.recherche.ModeGestionBean;
import org.cocktail.cocowork.server.metier.convention.service.recherche.RechercheContratService;
import org.cocktail.cocowork.server.metier.convention.service.recherche.ResultatRechercheBean;
import org.cocktail.cocowork.server.metier.convention.service.recherche.ServiceGestionnaireBean;
import org.cocktail.cocowork.server.metier.convention.service.recherche.TypeContratBean;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXS;

/**
 * 
 * Page de recherche avancée.
 * 
 * @author Alexis Tual
 *
 */
public class Recherche extends MyWOComponent {

    private static final long serialVersionUID = 1L;

    private ERXDisplayGroup<ResultatRechercheBean> dgContrats;

    private ResultatRechercheBean currentResultat;

    private ServiceGestionnaireBean currentService;
    private NSArray<ServiceGestionnaireBean> lesServices;
    private DisciplineBean currentDiscipline;
    private NSArray<DisciplineBean> lesDisciplines;
    private TypeContratBean currentTypeContrat;
    private NSArray<TypeContratBean> lesTypesContrat;
    private ModeGestionBean currentModeDeGestion;
    private NSArray<ModeGestionBean> lesModesDeGestion;
    @Inject
    private RechercheContratService rechercheContratService;

    private NSArray<ResultatRechercheBean> allResultats;
    private Filtre filtre;

    /**
     * @param context un contexte
     */
    public Recherche(WOContext context) {
        super(context);
        filtre = new Filtre();
    }

    /**
     * Affiche la liste des conventions de l'utilisateur filtrée selon les critères sélectionnés.
     * 
     * @return null
     */
    public WOActionResults rechercher() {
        if (allResultats == null) {
            boolean voirToutes = session().applicationUser().hasDroitSuperAdmin() 
                                     || session().applicationUser().hasDroitConsultationTousLesContratsEtAvenants();
            allResultats = rechercheContratService.conventionsForUtilisateur(
                    edc(), 
                    session().applicationUser().getUtilisateur().utlOrdre(),
                    session().applicationUser().getNoIndividu(),
                    session().applicationUser().getPersId(),
                    voirToutes);
        }
        NSArray<ResultatRechercheBean> resultatsFiltres 
                        = rechercheContratService.filtrerConventions(allResultats, filtre);
        dgContrats().setObjectArray(resultatsFiltres);
        return null;
    }

    /**
     * Remet à zéro les critères sélectionnés et réaffiche les conventions de l'utilisateur.
     * 
     * @return null
     */
    public WOActionResults nettoyer() {
        filtre = new Filtre();
        return rechercher();
    }

    /**
     * Affiche la convention sélectionnée.
     * 
     * @return une page {@link Convention}
     */
    public WOActionResults consulterContrat() {
        Convention nextPage = null;
        ResultatRechercheBean resultat = dgContrats().selectedObject();
        if (resultat != null) {
            Contrat contrat = (Contrat)EOUtilities.objectWithPrimaryKeyValue(edc(), Contrat.ENTITY_NAME, resultat.getConOrdre());
            nextPage = (Convention)pageWithName(Convention.class.getName());
            nextPage.setPageDeRecherche(this);
            nextPage.setConvention(contrat);
            session().setContrat(contrat);
            session().setPageConvention(nextPage);
        }
        ERXRedirect redirect = (ERXRedirect)pageWithName(ERXRedirect.class.getName());
        redirect.setComponent(nextPage);
        return redirect;
    }

    /**
     * @return null
     */
    public WOActionResults submit() {
        return null;
    }

    public String getIndexSortKeyPath() {
        return ResultatRechercheBean.INDEX_KEY;
    }
    
    public String getObjetSortKeyPath() {
        return ResultatRechercheBean.OBJET_KEY;
    }
    
    public String getServiceGestSortKeyPath() {
        return ResultatRechercheBean.SERVICE_GEST_KEY + "." + ServiceGestionnaireBean.LIBELLE;
    }
    
    /**
     * @return the dgContrats
     */
    public ERXDisplayGroup<ResultatRechercheBean> dgContrats() {
        if (dgContrats == null) {
            dgContrats = new ERXDisplayGroup<ResultatRechercheBean>();
            dgContrats().setSortOrderings(ERXS.descs(ResultatRechercheBean.INDEX_KEY));
            dgContrats.setSelectsFirstObjectAfterFetch(false);
        }
        return dgContrats;
    }

    public void setDgContrats(ERXDisplayGroup<ResultatRechercheBean> dgContrats) {
        this.dgContrats = dgContrats;
    }

    public ResultatRechercheBean getCurrentResultat() {
        return currentResultat;
    }

    public void setCurrentResultat(ResultatRechercheBean currentResultat) {
        this.currentResultat = currentResultat;
    }

    /**
     * @return the lesModesDeGestion
     */
    public NSArray<ModeGestionBean> lesModesDeGestion() {
        if (lesModesDeGestion == null) {
            lesModesDeGestion = rechercheContratService.modesGestionFromResultats(dgContrats().displayedObjects());
        }
        return lesModesDeGestion;
    }

    /**
     * @return the lesDisciplines
     */
    public NSArray<DisciplineBean> lesDisciplines() {
        if (lesDisciplines == null) {
            lesDisciplines = rechercheContratService.disciplinesFromResultats(dgContrats().displayedObjects());
        }
        return lesDisciplines;
    }

    public boolean isAfficherDetailDisabled() {
        return !isAfficherDetailEnabled();
    }
    
    /**
     * @return true si on affiche le détail du contrat
     */
    public boolean isAfficherDetailEnabled() {
        boolean isAfficherDetailEnabled = dgContrats != null && dgContrats.selectedObject() != null ? true : false;
        return isAfficherDetailEnabled;
    }

    /**
     * @return une page {@link Accueil}
     */
    public WOActionResults accueil() {
        Accueil nextPage = (Accueil) pageWithName(Accueil.class.getName());
        session().reset();
        return nextPage;
    }

    /**
     * @return les types de contrat correspondant aux résultats
     */
    public NSArray<TypeContratBean> lesTypesContrat() {
        if (lesTypesContrat == null) {
            lesTypesContrat = rechercheContratService.typesContratFromResultats(dgContrats().displayedObjects());
        }
        return lesTypesContrat;
    }

    /**
     * @return les types de contrat correspondant aux services
     */
    public NSArray<ServiceGestionnaireBean> lesServices() {
        if (lesServices == null) {
            lesServices = rechercheContratService.servicesGestionnairesFromResultats(dgContrats().displayedObjects());
        }
        return lesServices;
    }

    public ServiceGestionnaireBean getCurrentService() {
        return currentService;
    }

    public void setCurrentService(ServiceGestionnaireBean currentService) {
        this.currentService = currentService;
    }

    public DisciplineBean getCurrentDiscipline() {
        return currentDiscipline;
    }

    public void setCurrentDiscipline(DisciplineBean currentDiscipline) {
        this.currentDiscipline = currentDiscipline;
    }

    public TypeContratBean getCurrentTypeContrat() {
        return currentTypeContrat;
    }

    public void setCurrentTypeContrat(TypeContratBean currentTypeContrat) {
        this.currentTypeContrat = currentTypeContrat;
    }

    public ModeGestionBean getCurrentModeDeGestion() {
        return currentModeDeGestion;
    }

    public void setCurrentModeDeGestion(ModeGestionBean currentModeDeGestion) {
        this.currentModeDeGestion = currentModeDeGestion;
    }

    public Filtre getFiltre() {
        return filtre;
    }

    public void setRechercheContratService(RechercheContratService rechercheContratService) {
        this.rechercheContratService = rechercheContratService;
    }
    
    /**
     * @return la classe css du contrat en cours
     */
    public String cssClassForCurrentContrat() {
        String css = "";
        Object date = currentResultat.getDateValidite();
        if (date != null && !date.equals(NSKeyValueCoding.NullValue)) {
            css = "valid";
        }
        
        Date dateFin = currentResultat.getDateFin();
        if (dateFin != null && !dateFin.equals(NSKeyValueCoding.NullValue) && dateFin.before(new Date())) {
            css = "terminee";
        }
        
        return css;
    }

}
