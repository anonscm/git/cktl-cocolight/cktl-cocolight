package org.cocktail.cocolight.serveur.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOActionResults;

public class Outils extends MyWOComponent {
    public Outils(WOContext context) {
        super(context);
    }

    public WOActionResults reportDesCredits() {
      return pageWithName(ReportDesCreditsEnMasse.class.getName());
    }
    
    public Boolean peutReporterLesCredits() {
    	return session().applicationUser().hasDroitSuperAdmin() || session().applicationUser().hasDroitReportCredits();
    }
}