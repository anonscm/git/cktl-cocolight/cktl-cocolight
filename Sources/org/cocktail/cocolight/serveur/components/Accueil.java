/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.cocolight.serveur.components;





import java.util.Date;

import org.cocktail.application.client.eof.EOExerciceCocktail;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.cocowork.server.metier.convention.service.recherche.Filtre;
import org.cocktail.cocowork.server.metier.convention.service.recherche.RechercheContratService;
import org.cocktail.cocowork.server.metier.convention.service.recherche.ResultatRechercheBean;
import org.cocktail.cocowork.server.metier.convention.service.recherche.ServiceGestionnaireBean;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSKeyValueCoding;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.appserver.ERXRedirect;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

public class Accueil extends MyWOComponent {

    private Boolean isOpenFenetreException = Boolean.FALSE;
    private ERXDisplayGroup<ResultatRechercheBean> dgContrats;
    private ResultatRechercheBean currentContrat;
    @Inject
    private RechercheContratService rechercheContratService;
    private Filtre filtre;
    


    public Accueil(WOContext context) {
        super(context);
        filtre = new Filtre();
    }

    private GestionConvention creerConvention(boolean isRA) {
        GestionConvention nextPage = (GestionConvention) pageWithName(GestionConvention.class.getName());
        EOEditingContext ec = context().session().defaultEditingContext();
        FactoryConvention fc = new FactoryConvention(ec, application().isModeDebug());
        Contrat contrat;
        try {
            // Ajout en tant que partenaire interne principal de l'etablissement du createur
            PersonneApplicationUser persAppUser = new PersonneApplicationUser(ec, applicationUser().getPersId());
            NSArray services = persAppUser.getServices();
            if (services != null) {
                NSArray etablissements = persAppUser.getEtablissementsAffectation();
                if (etablissements != null && etablissements.count() == 1) {
                    contrat = fc.creerConventionVierge(applicationUser().getUtilisateur(), (EOStructure) etablissements.lastObject(), (EOStructure) services.lastObject());
                    if (isRA) {
                        NSArray lesModesDeGestion = (NSArray) EOSharedEditingContext.defaultSharedEditingContext().objectsByEntityName().objectForKey(ModeGestion.ENTITY_NAME);
                        EOKeyValueQualifier qual = new EOKeyValueQualifier(ModeGestion.MG_LIBELLE_COURT_KEY, EOQualifier.QualifierOperatorEqual, "RA");
                        ModeGestion modeRA = (ModeGestion) EOQualifier.filteredArrayWithQualifier(lesModesDeGestion, qual).lastObject();
                        contrat.avenantZero().setModeGestionRelationship(modeRA);
                    }
                } else {
                    throw new IllegalStateException("Impossible de détecter votre établissement d'affectation");                
                }
                session().setContrat(contrat);
                nextPage.setContrat(contrat);
            } else {
                contrat = fc.creerConventionVierge(session().applicationUser().getUtilisateur());
                session().setContrat(contrat);
                nextPage.setContrat(contrat);
            }
        } catch (Exception e) {
            throw NSForwardException._runtimeExceptionForThrowable(e);
        }
        return nextPage;
    }

    public WOActionResults creerUneConvention() {
        return creerConvention(false);
    }

    public WOActionResults creerUneConventionRA() {
        return creerConvention(true);
    }

    public WOActionResults creerUnContrat() {
        return null;
    }
    
    private WOComponent traiterResultatsRechercheSimple(NSArray<ResultatRechercheBean> resultats) {
        Convention nextPage = null;
        if (resultats != null && resultats.size() == 1) {
            Contrat convention = (Contrat) EOUtilities.objectWithPrimaryKeyValue(edc(), Contrat.ENTITY_NAME, resultats.lastObject().getConOrdre());
            if (convention != null && convention.isConsultablePar(session().applicationUser().getUtilisateur())) {
                nextPage = (Convention) pageWithName(Convention.class.getName());
                nextPage.setConvention(convention);
                session().setContrat(convention);
                session().setPageConvention(nextPage);
            } else {
                session().setMessageErreur("Vous n'êtes pas autorisé(e) à consulter cette convention");
            }
        } else {
            session().setMessageErreur("Aucune convention trouvée");
        }
        return nextPage;
    }
    
    public WOActionResults rechercherUneConvention() {
        WOComponent nextPage = null;
        if (filtre.getFiltreIndex() != null && filtre.getFiltreExeordre() != null) {
            NSArray<ResultatRechercheBean> resultats = 
                    rechercheContratService.filtrerConventions(dgContrats().allObjects(), filtre);
            nextPage = traiterResultatsRechercheSimple(resultats);
        } else {
            session().setMessageErreur("Vous devez saisir l'exercice et le numéro de la convention");
        }
        return nextPage;
    }

    public WOActionResults submit() {
        return null;
    }

    public WOComponent retourAccueil() {
        Accueil accueil = (Accueil) session().getSavedPageWithName(Accueil.class.getName());
        session().reset();
        //accueil.setOnloadJS(null);
        accueil.setIsOpenFenetreException(Boolean.FALSE);
        return accueil;
    }

	public String getNumeroSortKeyPath() {
		return ResultatRechercheBean.NUMERO_POUR_TRI_KEY;
	}

    public String getObjetSortKeyPath() {
        return ResultatRechercheBean.OBJET_KEY;
    }
    
    public String getServiceGestSortKeyPath() {
        return ResultatRechercheBean.SERVICE_GEST_KEY + "." + ServiceGestionnaireBean.LIBELLE;
    }

    public WOActionResults afficherRechercheAvancee() {
        Recherche nextPage = (Recherche) pageWithName(Recherche.class.getName());
        nextPage.rechercher();
        return nextPage;
    }

    public ERXDisplayGroup<ResultatRechercheBean> dgContrats() {
        if (dgContrats == null) {
            dgContrats = new ERXDisplayGroup<ResultatRechercheBean>();
            dgContrats.setSelectsFirstObjectAfterFetch(false);
            boolean voirToutes = session().applicationUser().hasDroitSuperAdmin() 
                    || session().applicationUser().hasDroitConsultationTousLesContratsEtAvenants();
            NSArray<ResultatRechercheBean> resultats = rechercheContratService.conventionsForUtilisateur(
                    edc(), 
                    session().applicationUser().getUtilisateur().utlOrdre(),
                    session().applicationUser().getNoIndividu(),
                    session().applicationUser().getPersId(),
                    voirToutes);
            dgContrats.setSortOrderings(ERXS.descs(ResultatRechercheBean.NUMERO_POUR_TRI_KEY));
            dgContrats.setObjectArray(resultats);
        }
        return dgContrats;
    }

    public WOActionResults consulterContrat() {
        Convention nextPage = null;
        ResultatRechercheBean resultat = dgContrats().selectedObject();
        if (resultat != null) {
            Contrat contrat = (Contrat) EOUtilities.objectWithPrimaryKeyValue(edc(), Contrat.ENTITY_NAME, resultat.getConOrdre());
            nextPage = (Convention) pageWithName(Convention.class.getName());
            nextPage.setConvention(contrat);
            session().setContrat(contrat);
            session().setPageConvention(nextPage);
        }
        ERXRedirect redirect = (ERXRedirect) pageWithName(ERXRedirect.class.getName());
        redirect.setComponent(nextPage);
        return redirect;
    }

    public boolean isAfficherDetailDisabled() {
        return !isAfficherDetailEnabled();
    }
    
    public boolean isAfficherDetailEnabled() {
        boolean isAfficherDetailEnabled = dgContrats != null && dgContrats.selectedObject() != null;
        return isAfficherDetailEnabled;
    }

    public boolean isCreerUneConventionDisabled() {
        return !applicationUser().hasDroitCreationContratsEtAvenants();
    }

    /**
     * @return the isOpenFenetreException
     */
    public Boolean isOpenFenetreException() {
        return isOpenFenetreException;
    }

    /**
     * @param isOpenFenetreException the isOpenFenetreException to set
     */
    public void setIsOpenFenetreException(Boolean isOpenFenetreException) {
    	this.isOpenFenetreException = isOpenFenetreException;
    }

    public boolean isRechercherDisabled() {
    	return filtre.getFiltreIndex() == null || filtre.getFiltreExeordre() == null;
    }

    public String cssClassForCurrentContrat() {
    	String css = "";


    	Object date = currentContrat.getDateValidite();
    	if (date != null && !date.equals(NSKeyValueCoding.NullValue)) {
    		css = "valid";
    	}
    	
    	Date aujourdhui = new Date();
    	Date dateReelle = calculeDateReelle();
    	
		if (dateReelle != null && dateReelle.before(aujourdhui)) {
    		css = "terminee";
    	}
    
		return css;
    }

	/**
	 * @param css
	 * @param aujourdhui
	 * @return
	 */
	private Date calculeDateReelle() {
		Date dateFin = currentContrat.getDateFin();
		Date aujourdhui = new Date();
		if (dateFin != null && !dateFin.equals(NSKeyValueCoding.NullValue) && dateFin.before(aujourdhui)) {
			// Si convention initiale est terminée alors recherche s'il existe des avenants avec une datefin non
			// dépassée relative à la convention en cours
			ERXFetchSpecification<EOEnterpriseObject> fetchSpec = new ERXFetchSpecification<EOEnterpriseObject>(Contrat.ENTITY_NAME);
			fetchSpec.setFetchesRawRows(true);

			EOQualifier qual_conOrdre = ERXQ.equals(Contrat.CON_ORDRE_KEY, currentContrat.getConOrdre());
			EOQualifier qual_exeOrdre = ERXQ.equals(Contrat.EXERCICE_COCKTAIL_KEY + "." + EOExerciceCocktail.EXE_ORDRE_KEY, currentContrat.getExeOrdre());
			EOQualifier qual_finNotNull = ERXQ.isNotNull(Contrat.AVENANTS.dot(Avenant.AVT_DATE_FIN).key());
			ERXSortOrderings sort = Contrat.AVENANTS.dot(Avenant.AVT_INDEX).descs();

			EOQualifier qual = ERXQ.and(qual_conOrdre, qual_exeOrdre, qual_finNotNull);
			fetchSpec.setQualifier(qual);
			fetchSpec.setSortOrderings(sort);
			fetchSpec.setFetchLimit(1);

			fetchSpec.setRawRowKeyPaths(Contrat.AVENANTS.dot(Avenant.AVT_DATE_FIN).key());
			EOEditingContext ec = context().session().defaultEditingContext();
			NSArray<NSDictionary<String, Object>> resultats = fetchSpec.fetchRawRows(ec);
			dateFin = (Date) resultats.objectAtIndex(0).valueForKey(Contrat.AVENANTS.dot(Avenant.AVT_DATE_FIN_KEY).key());
			// si tous les avenants sont terminés on marquera la ligne avec un style différent.

		}
		return dateFin;
	}

    public ResultatRechercheBean getCurrentContrat() {
        return currentContrat;
    }
    
    public void setCurrentContrat(ResultatRechercheBean currentContrat) {
        this.currentContrat = currentContrat;
    }

    public WOActionResults afficherLesEditions() {
      GestionEditions gestionEditions = (GestionEditions) pageWithName(GestionEditions.class.getName());
      return gestionEditions;
    }

    public WOActionResults afficherLesOutils() {
      return pageWithName(Outils.class.getName());
    }
    
    public Filtre getFiltre() {
        return filtre;
    }

}
