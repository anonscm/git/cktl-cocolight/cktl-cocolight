package org.cocktail.cocolight.serveur.components;

import java.math.BigDecimal;
import java.util.Calendar;

import org.cocktail.cocowork.common.tools.Constantes;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.cocowork.server.metier.convention.depenses.ConventionsDepensesService;
import org.cocktail.cocowork.server.metier.convention.service.ReportDesCreditsService;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOMandat;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXDictionaryUtilities;
import er.extensions.foundation.ERXProperties;

public class ReportDesCreditsEnMasse extends MyWOComponent {


	
	
	private EOExercice exerciceSourcePourLeReport;
	private EOExercice exerciceDestinationPourLeReport;

	private EOEditingContext edc;

	private String logDeSimlation;

	private NSArray<Contrat> conventionsSimplesAReporter;
	
	String rouge = "#cc0000";
	String vert = "#73d216";
	String orange = "#fcaf3e";
	
	public Integer STATUT_REPORT_OUI = ReportDesCreditsService.STATUT_REPORT_OUI;
	public Integer STATUT_REPORT_OUI_MAIS = ReportDesCreditsService.STATUT_REPORT_OUI_MAIS;
	public Integer STATUT_REPORT_NON = ReportDesCreditsService.STATUT_REPORT_NON;
	
	private NSMutableDictionary<Contrat, BigDecimal> montantsReports;
	private NSMutableDictionary<Contrat, String> messagesContrats;
	private NSMutableDictionary<Contrat, Integer> statutsReports;
	
	private ERXDisplayGroup<Contrat> contratsDisplayGroup;
	private Contrat currentContrat;
	private NSArray<Contrat> contratsPourLeReport;
	private EOExercice currentExercice;
	
	
	private String detailsMessage;
	
	
	private NSArray<NSDictionary<String, Object>> donneesMontantsDepensesConventionsSimples;
	private NSArray<NSDictionary<String, Object>> donneesMontantsDepensesConventionsRa;
	private NSArray<NSDictionary<String, Object>> donneesResteEngageConventionsSimples;
	private NSArray<NSDictionary<String, Object>> donneesResteEngageConventionsRa;
	private NSArray<NSDictionary<String, Object>> donneesEtatsMandatsConventionsSimples;
	private NSArray<NSDictionary<String, Object>> donneesEtatsMandatsConventionsRa;
	
	public ReportDesCreditsEnMasse(WOContext context) {
		super(context);
	}
	
	public String detailsWindowId = getComponentId() + "_detailsWindow";

	@Override
	public EOEditingContext edc() {
		if (edc == null) {
			edc = ERXEC.newEditingContext();
		}
		return edc;
	}
	
	public NSArray<EOExercice> exercices() {
		return FinderExercice.getExercices(edc());
	}
	
	
	

	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public void setCurrentExercice(EOExercice currentExercice) {
		this.currentExercice = currentExercice;
	}

    public EOExercice exerciceSourcePourLeReport() {
    	
        if(exerciceSourcePourLeReport == null) {

        	exerciceSourcePourLeReport = ReportDesCreditsService.creerNouvelleInstance(edc()).exerciceSourcePourLeReport();
    	
        }

        return exerciceSourcePourLeReport;
        
      }
      
      public EOExercice exerciceDestinationPourLeReport() {
        if(exerciceDestinationPourLeReport == null) {
          exerciceDestinationPourLeReport = ReportDesCreditsService.creerNouvelleInstance(edc()).exerciceDestinationPourLeReport();
        }
        return exerciceDestinationPourLeReport;
      } 


	public String currentContratTrancheSuivante() {
		
		
		Integer statut = getStatutsReports().get(getCurrentContrat());
		if(statut == STATUT_REPORT_NON) {
			return stylePourIndicateurTranche(rouge);
		} else if (statut == STATUT_REPORT_OUI_MAIS) {
			return stylePourIndicateurTranche(orange);
		} else {
			return stylePourIndicateurTranche(vert);
		}
	}
	
	public String stylePourIndicateurTranche(String couleur) {
		String style = "text-align: center; background-color: ";
		return style + couleur + ";";

	}
	
	
	public String stylePourLegende(String couleur) {
		String style = "border: black thin solid; width: 20px; display: inline-block; background-color: ";
		return style + couleur + ";";
	}
	
	public String stylePourLegendeRouge() {
		return stylePourLegende(rouge);
	}
	
	public String stylePourLegendeOrange() {
		return stylePourLegende(orange);
	}
	
	public String stylePourLegendeVert() {
		return stylePourLegende(vert);
	}

	public Boolean gestionTrancheAuto() {
		return ERXProperties.booleanForKeyWithDefault(
				Constantes.GESTION_TRANCHE_AUTO_PARAM, true);
	}

	public WOActionResults simuler() {
		StringBuffer buffer = new StringBuffer();
		
		contratsPourLeReport = new NSMutableArray<Contrat>();
		
		NSArray<Contrat> contrats = getContratsDisplayGroup().selectedObjects();
		for(Contrat contrat : contrats) {
			Integer statut = getStatutsReports().objectForKey(contrat);
			if(statut != STATUT_REPORT_NON) {
				contratsPourLeReport.add(contrat);
				buffer.append("[" + contrat.exerciceEtIndex() + "] : ");
				buffer.append(getMontantsReports().get(contrat)
						+ " € seront reportés de la tranche "
						+ exerciceSourcePourLeReport().exeExercice());
				buffer.append(" vers la tranche "
						+ exerciceDestinationPourLeReport().exeExercice());
				buffer.append("<br/>");
			} 
			if(statut == STATUT_REPORT_OUI_MAIS) {
				buffer.append("[" + contrat.exerciceEtIndex() + "] : ");
				buffer.append(" la tranche "
						+ exerciceDestinationPourLeReport()
								.exeExercice() + " sera créée<br/>");
			}
		
		}
		logDeSimlation = buffer.toString();
		return doNothing();
	}

	public String logDeSimlation() {
		return logDeSimlation;
	}

	public WOActionResults valider() {
		for (Contrat contrat: getContratsPourLeReport()) {
			Tranche tranche = contrat
					.tranchePourExercice(
							EOExerciceCocktail.fetchByQualifier(
									edc(),
									ERXQ.equals(
											EOExerciceCocktail.EXE_EXERCICE_KEY,
											exerciceSourcePourLeReport()
													.exeExercice())), false);
			try {
				Tranche.effectuerReportDeCredits(
						tranche, 
						getMontantsReports().get(contrat), 
						edc(),
						session().applicationUser().getUtilisateur().localInstanceIn(edc())
					);
			} catch (Exception e) {
				session().addSimpleErrorMessage("Erreur", e);
			}
		}
		try {
			edc().saveChanges();
			contratsDisplayGroup = null;
			contratsPourLeReport = new NSMutableArray<Contrat>();
		} catch (ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
			edc().revert();
		}
		return doNothing();
	}

	public Boolean nePeutValider() {
		if(getContratsPourLeReport() == null) return true;
		return getContratsPourLeReport().isEmpty();
	}
	
	
	

	
	public NSArray<Contrat> traiterLesConventions() {
		
		NSMutableArray<Contrat> contratsAvecReport = new NSMutableArray<Contrat>(); 
		
		EOQualifier qualifierContratsAvecTrancheSurExerciceSource = 
				Contrat.TRANCHES.dot(Tranche.EXERCICE_COCKTAIL)
				.dot(EOExerciceCocktail.EXE_EXERCICE_KEY)
				.containsObject(exerciceSourcePourLeReport().exeExercice())
				.and(Contrat.TRANCHES.dot(Tranche.TRA_SUPPR).eq(Tranche.TRA_SUPPR_NON))
				.and(Contrat.TRANCHES.dot(Tranche.TRA_VALIDE).eq(Tranche.TRA_VALIDE_OUI))
				.and(Contrat.TRANCHES.dot(Tranche.REPORT_NPLUS1).eq(BigDecimal.ZERO));
		
		
		
	      
		NSMutableArray<String> paths = new NSMutableArray<String>();
		paths.add(Contrat.TRANCHES.key());
	    paths.add(Contrat.TRANCHES.dot(Tranche.TO_REPART_PARTENAIRE_TRANCHES).dot(RepartPartenaireTranche.FRAIS_GESTIONS).key());
//	    paths.add(Contrat.TRANCHES
//	    			.dot(Tranche.HISTO_CREDIT_POSITIONNES)
//	    			.dot(HistoCreditPositionne.ENGAGEMENT_BUDGETS)
//	    			.dot(EOEngagementBudget.DEPENSE_BUDGETS)
//	    			.dot(EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES)
//	    			.dot(EODepenseControlePlanComptable.MANDAT).key());
//	    paths.add(Contrat.DEPENSE_CONTROLE_CONVENTIONS.dot(EODepenseControleConvention.EXERCICE).key());
	    paths.add(Contrat.AVENANTS .dot(Avenant.MODE_GESTION).key());
//	    paths.add(Contrat.DEPENSE_CONTROLE_CONVENTIONS
//	    		  .dot(EODepenseControleConvention.DEPENSE_BUDGET_KEY)
//	    		  .dot(EODepenseBudget.ENGAGEMENT_BUDGET).key());
//	    paths.add(Contrat.DEPENSE_CONTROLE_CONVENTIONS
//	    		  .dot(EODepenseControleConvention.DEPENSE_BUDGET)
//	    		  .dot(EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES)
//	    		  .dot(EODepenseControlePlanComptable.MANDAT).key());
	    
		ERXFetchSpecification<Contrat> fetchSpecification = new ERXFetchSpecification<Contrat>(Contrat.ENTITY_NAME, qualifierContratsAvecTrancheSurExerciceSource, null);
	    fetchSpecification.setPrefetchingRelationshipKeyPaths(paths);
	    NSArray<Contrat> contrats = fetchSpecification.fetchObjects(edc());
	    
	    initialiserDonneesMontantsDepensesConventionsSimples(contrats, exerciceSourcePourLeReport());
	    initialiserDonneesMontantsDepensesConventionsRa(contrats, exerciceSourcePourLeReport());
	    initialiserDonneesResteEngageConventionsSimples(contrats, exerciceSourcePourLeReport());
	    initialiserDonneesResteEngageConventionsRa(contrats, exerciceSourcePourLeReport());
	    initialiserDonneesEtatsMandatsConventionSimples(contrats, exerciceSourcePourLeReport());
	    initialiserDonneesEtatsMandatsConventionsRa(contrats, exerciceSourcePourLeReport());
	    
	    
	    ConventionsDepensesService conventionsDepensesService = ConventionsDepensesService.creerNouvelleInstance();
	    ReportDesCreditsService reportDesCreditsService = ReportDesCreditsService.creerNouvelleInstance(edc());
	    for(Contrat contrat : contrats) {
	    	BigDecimal montantReport = conventionsDepensesService.totalReportablePourConventionSurExercice(contrat, exerciceSourcePourLeReport());
	    	if(montantReport.signum() == 1) {
	    		getMontantsReports().put(contrat, montantReport);
	    		if(getTousLesEngagementsSontSoldes(contrat) == false) {
	    			getMessagesContrats().put(contrat, "Il y a des engagements non soldés en lien avec cette convention");
	    			getStatutsReports().put(contrat, STATUT_REPORT_NON);
	    		} else if(getTousLesDepensesOntUnBordereau(contrat) == false) {
	    			getMessagesContrats().put(contrat, "Il y a des dépenses sans bordereau en lien avec cette convention");
	    			getStatutsReports().put(contrat, STATUT_REPORT_NON);
	    		} else if(getTousLesMandatsSontVises(contrat) == false) {
	    			getMessagesContrats().put(contrat, "Il y a des dépenses avec des mandats non visés en lien avec cette convention");
	    			getStatutsReports().put(contrat, STATUT_REPORT_NON);
	    		} else {
	    			Integer statut = reportDesCreditsService.creditsReportablesPourConventionSurExercice(contrat, exerciceDestinationPourLeReport());
	    			if(statut == STATUT_REPORT_NON) {
	    				getMessagesContrats().put(contrat, "L'exercice de destination est en dehors des dates du contrat");
	    			} else if(statut == STATUT_REPORT_OUI_MAIS) {
	    				getMessagesContrats().put(contrat, "La tranche suivante sera créée");
	    			}
	    			getStatutsReports().put(contrat, statut);
	    		} 
	    		contratsAvecReport.add(contrat);
	    	}
	    }
	    
	    setDonneesEtatsMandatsConventionsRa(null);
	    setDonneesEtatsMandatsConventionsSimples(null);
	    setDonneesMontantsDepensesConventionsRa(null);
	    setDonneesMontantsDepensesConventionsSimples(null);
	    setDonneesResteEngageConventionsRa(null);
	    setDonneesResteEngageConventionsSimples(null);
	    
	    return contratsAvecReport;
	}

	
	public NSArray<Contrat> getConventionsSimplesAReporter() {
		return conventionsSimplesAReporter;
	}

	public void setConventionsSimplesAReporter(
			NSArray<Contrat> conventionsSimplesAReporter) {
		this.conventionsSimplesAReporter = conventionsSimplesAReporter;
	}



	public NSMutableDictionary<Contrat, BigDecimal> getMontantsReports() {
		if(montantsReports == null) {
			montantsReports = new NSMutableDictionary<Contrat, BigDecimal>();
		}
		return montantsReports;
	}

	public ERXDisplayGroup<Contrat> getContratsDisplayGroup() {
		if(contratsDisplayGroup == null) {
			contratsDisplayGroup = new ERXDisplayGroup<Contrat>();
			contratsDisplayGroup.setObjectArray(traiterLesConventions());
			contratsDisplayGroup.setSortOrderings(Contrat.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).asc().then(Contrat.CON_INDEX.asc()));
			contratsDisplayGroup.fetch();
		}
		return contratsDisplayGroup;
	}

	public void setContratsDisplayGroup(ERXDisplayGroup<Contrat> contratsDisplayGroup) {
		this.contratsDisplayGroup = contratsDisplayGroup;
	}

	public Contrat getCurrentContrat() {
		return currentContrat;
	}

	public void setCurrentContrat(Contrat currentContrat) {
		this.currentContrat = currentContrat;
	}
	
	public BigDecimal getCurrentMontant() {
		return getMontantsReports().objectForKey(getCurrentContrat());
	}

	public Tranche getCurrentTrancheSuivante() {
		return trancheSuivante(getCurrentContrat());
	}
	
	public Boolean getCurrentTrancheSuivanteExiste() {
		return getCurrentTrancheSuivante() == null;
	}

	public NSArray<Contrat> getContratsPourLeReport() {
		return contratsPourLeReport;
	}

	public void setContratsPourLeReport(NSArray<Contrat> contratsPourLeReport) {
		this.contratsPourLeReport = contratsPourLeReport;
	}
	
	public Tranche trancheSuivante(Contrat contrat) {
		NSArray<Tranche>  tranches = contrat.tranches(Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exerciceDestinationPourLeReport().exeExercice()));
		if(tranches.isEmpty()) {
			return null;
		} else {
			return ERXArrayUtilities.firstObject(tranches);
		}
	}
	
	
	public Boolean trancheDansLesDatesDuContrat(Tranche tranche, Contrat contrat) {
		
		if(tranche == null) {
			return false;
		}
		
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(contrat.dateDebut());
		Integer anneeDebut  = cal.get(Calendar.YEAR);
		
		cal.setTime(contrat.dateFin());
		Integer anneeFin = cal.get(Calendar.YEAR);

		Integer exerciceTranche = tranche.exerciceCocktail().exeExercice();
		
		return anneeDebut <= exerciceTranche && exerciceTranche <= anneeFin;
	}

	public NSMutableDictionary<Contrat, String> getMessagesContrats() {
		if(messagesContrats == null) {
			messagesContrats = new NSMutableDictionary<Contrat, String>(); 
		}
		return messagesContrats;
	}

	public void setMessagesContrats(NSMutableDictionary<Contrat, String> messagesContrats) {
		this.messagesContrats = messagesContrats;
	}

	public NSMutableDictionary<Contrat, Integer> getStatutsReports() {
		if(statutsReports == null) {
			statutsReports = new NSMutableDictionary<Contrat, Integer>(); 
		}
		return statutsReports;
	}

	public void setStatutsReports(NSMutableDictionary<Contrat, Integer> statutsReports) {
		this.statutsReports = statutsReports;
	}
	
	public WOActionResults montrerDetails() {
		setDetailsMessage(getMessagesContrats().objectForKey(getCurrentContrat()));
		return doNothing();
	}
	
	public Boolean afficherFenetreMessage() {
		return STATUT_REPORT_NON.equals(getStatutsReports().objectForKey(getCurrentContrat()));
	}

	public String getDetailsMessage() {
		return detailsMessage;
	}

	public void setDetailsMessage(String detailsMessage) {
		this.detailsMessage = detailsMessage;
	}
	
	public WOActionResults recharger() {
		setContratsDisplayGroup(null);
		return doNothing();
	}
	
	
	
	
	
	

	
	public ERXKey<Integer> MONTANTS_SIMPLE_CON_ORDRE = new ERXKey<Integer>(Contrat.CON_ORDRE_KEY);
	public ERXKey<BigDecimal> MONTANTS_SIMPLE_MONTANT_BUDGETAIRE = Contrat.DEPENSE_CONTROLE_CONVENTIONS
			.dot(EODepenseControleConvention.DCON_MONTANT_BUDGETAIRE);
	
	public void initialiserDonneesMontantsDepensesConventionsSimples(NSArray<Contrat> conventions, EOExercice exercice) {
		NSArray<Contrat> conventionsSimple = ERXQ.filtered(conventions, ERXQ.isFalse("isModeRA"));

		NSArray<Integer> conOrdres = (NSArray<Integer>) conventionsSimple.valueForKey(Contrat.CON_ORDRE.key());
		
		EOQualifier qualifier = 
				Contrat.CON_ORDRE.in(conOrdres)
				.and(
					Contrat.DEPENSE_CONTROLE_CONVENTIONS
					.dot(EODepenseControleConvention.EXERCICE)
					.dot(EOExercice.EXE_EXERCICE_KEY).eq(exercice.exeExercice().intValue()));
		
		ERXFetchSpecification<Contrat> contratFetchSpecification = 
				new ERXFetchSpecification<Contrat>(
						Contrat.ENTITY_NAME, 
						qualifier,
						null);
		
		NSArray<String> keyPaths = new NSMutableArray<String>();
		
		keyPaths.add(MONTANTS_SIMPLE_CON_ORDRE.key());
		keyPaths.add(
				MONTANTS_SIMPLE_MONTANT_BUDGETAIRE.key()
			); 

		contratFetchSpecification.setRawRowKeyPaths(keyPaths);
		NSArray<NSDictionary<String, Object>> conventionsFiltrees = contratFetchSpecification.fetchRawRows(edc());
		conventionsFiltrees = ERXQ.filtered(conventionsFiltrees, MONTANTS_SIMPLE_MONTANT_BUDGETAIRE.isNotNull());

		
		setDonneesMontantsDepensesConventionsSimples(conventionsFiltrees);
		
	}
	
	public ERXKey<Integer> MONTANTS_RA_CON_ORDRE = new ERXKey<Integer>(Contrat.CON_ORDRE_KEY);
	
	public ERXKey<BigDecimal> MONTANTS_RA_DEP_MONTANT_BUGETAIRE = Contrat.TRANCHES
			.dot(Tranche.HISTO_CREDIT_POSITIONNES)
			.dot(HistoCreditPositionne.ENGAGEMENT_BUDGETS)
			.dot(EOEngagementBudget.DEPENSE_BUDGETS)
			.dot(EODepenseBudget.DEP_MONTANT_BUDGETAIRE);
	
	public ERXKey<Integer> MONTANTS_RA_DEP_ID = Contrat.TRANCHES
			.dot(Tranche.HISTO_CREDIT_POSITIONNES)
			.dot(HistoCreditPositionne.ENGAGEMENT_BUDGETS)
			.dot(EOEngagementBudget.DEPENSE_BUDGETS)
			.dot(EODepenseBudget.DEP_ID_KEY);
	
	public ERXKey<String> MONTANTS_RA_HCP_SUPPR = Contrat.TRANCHES
			.dot(Tranche.HISTO_CREDIT_POSITIONNES)
			.dot(HistoCreditPositionne.HCP_SUPPR);
	
	public void initialiserDonneesMontantsDepensesConventionsRa(NSArray<Contrat> conventions, EOExercice exercice) {
		NSArray<Contrat> conventionsRa = ERXQ.filtered(conventions, ERXQ.isTrue("isModeRA"));
		NSArray<Integer> conOrdres = (NSArray<Integer>) conventionsRa.valueForKey(Contrat.CON_ORDRE.key());
		
		EOQualifier qualifier = 
				Contrat.CON_ORDRE.in(conOrdres)
				.and(
					Contrat.TRANCHES
					.dot(Tranche.EXERCICE_COCKTAIL)
					.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice().intValue())
				)
				.and(Contrat.TRANCHES.dot(Tranche.TRA_SUPPR).eq(Tranche.TRA_SUPPR_NON));
		
		ERXFetchSpecification<Contrat> contratFetchSpecification = 
				new ERXFetchSpecification<Contrat>(
						Contrat.ENTITY_NAME, 
						qualifier,
						null);
		
		NSArray<String> keyPaths = new NSMutableArray<String>();
		
		keyPaths.add(MONTANTS_RA_CON_ORDRE.key());
		keyPaths.add(MONTANTS_RA_DEP_MONTANT_BUGETAIRE.key()); 
		keyPaths.add(MONTANTS_RA_DEP_ID.key()); 
		keyPaths.add(MONTANTS_RA_HCP_SUPPR.key()); 

		contratFetchSpecification.setRawRowKeyPaths(keyPaths);
		
		NSArray<NSDictionary<String, Object>> conventionsFiltrees = contratFetchSpecification.fetchRawRows(edc());
		conventionsFiltrees = ERXQ.filtered(conventionsFiltrees, MONTANTS_RA_HCP_SUPPR.eq("N"));
		conventionsFiltrees = ERXArrayUtilities.arrayWithoutDuplicateKeyValue(conventionsFiltrees, MONTANTS_RA_DEP_ID.key());
		conventionsFiltrees = ERXQ.filtered(conventionsFiltrees, MONTANTS_RA_DEP_MONTANT_BUGETAIRE.isNotNull());
		setDonneesMontantsDepensesConventionsRa(conventionsFiltrees);
		
	}
	
	
	public BigDecimal getTotalDepensesPourConvention(Contrat convention) {
		
		BigDecimal total = BigDecimal.ZERO;
		
		if(convention.isModeRA()) {
			NSArray<NSDictionary<String, Object>> depensesContrat = 
					ERXQ.filtered(getDonneesMontantsDepensesConventionsRa(), MONTANTS_RA_CON_ORDRE.eq(convention.conOrdre()));
			total = (BigDecimal) depensesContrat.valueForKey(ERXKey.sum(MONTANTS_RA_DEP_MONTANT_BUGETAIRE).key());
		} else {
			NSArray<NSDictionary<String, Object>> depensesContrat = 
					ERXQ.filtered(getDonneesMontantsDepensesConventionsSimples(), MONTANTS_SIMPLE_CON_ORDRE.eq(convention.conOrdre()));
			total = (BigDecimal) depensesContrat.valueForKey(ERXKey.sum(MONTANTS_SIMPLE_MONTANT_BUDGETAIRE).key());
		}
		
		return total;
	}
	
	
	public ERXKey<Integer> RESTE_SIMPLE_CON_ORDRE = new ERXKey<Integer>(Contrat.CON_ORDRE_KEY);
	public ERXKey<Integer> RESTE_SIMPLE_MONTANT_RESTE = new ERXKey<Integer>("montantReste");
	
	public void initialiserDonneesResteEngageConventionsSimples(NSArray<Contrat> conventions, EOExercice exercice) {
		NSArray<Contrat> conventionsSimple = ERXQ.filtered(conventions, ERXQ.isFalse("isModeRA"));

		NSArray<Integer> conOrdres = (NSArray<Integer>) conventionsSimple.valueForKey(Contrat.CON_ORDRE.key());
		
		
		EOQualifier qualifierEngagements = 
				Contrat.CON_ORDRE.in(conOrdres)
				.and(Contrat.ENGAGEMENT_CONTROLE_CONVENTIONS.dot(EOEngagementControleConvention.EXERCICE).dot(EOExercice.EXE_EXERCICE_KEY).eq(exercice.exeExercice().intValue()));
		
		ERXFetchSpecification<Contrat> contratEngagementControleFetchSpecification = 
				new ERXFetchSpecification<Contrat>(
						Contrat.ENTITY_NAME, 
						qualifierEngagements,
						null);
		
		NSArray<String> keyPathsEngagement = new NSMutableArray<String>();
		
		keyPathsEngagement.add(Contrat.CON_ORDRE_KEY);
		
		ERXKey<BigDecimal> ENGAGEMENTS_MONTANT_RESTE = Contrat.ENGAGEMENT_CONTROLE_CONVENTIONS
		.dot(EOEngagementControleConvention.ECON_MONTANT_BUDGETAIRE_RESTE);
		
		keyPathsEngagement.add(
				ENGAGEMENTS_MONTANT_RESTE.key()
			); 

		contratEngagementControleFetchSpecification.setRawRowKeyPaths(keyPathsEngagement);
		
		NSArray<NSDictionary<String, Object>> donneesEngagements = contratEngagementControleFetchSpecification.fetchRawRows(edc());
		
		EOQualifier qualifierDepenses = 
				Contrat.CON_ORDRE.in(conOrdres)
				.and(Contrat.DEPENSE_CONTROLE_CONVENTIONS.dot(EODepenseControleConvention.EXERCICE).dot(EOExercice.EXE_EXERCICE_KEY).eq(exercice.exeExercice().intValue()));
		
		ERXFetchSpecification<Contrat> contratDepenseControleFetchSpecification = 
				new ERXFetchSpecification<Contrat>(
						Contrat.ENTITY_NAME, 
						qualifierDepenses,
						null);
		
		NSArray<String> keyPathsDepenses = new NSMutableArray<String>();
		
		keyPathsDepenses.add(Contrat.CON_ORDRE_KEY);
		
		ERXKey<BigDecimal> DEPENSES_MONTANT_RESTE = Contrat.DEPENSE_CONTROLE_CONVENTIONS
		.dot(EODepenseControleConvention.DEPENSE_BUDGET)
		.dot(EODepenseBudget.ENGAGEMENT_BUDGET)
		.dot(EOEngagementBudget.ENG_MONTANT_BUDGETAIRE_RESTE);
		
		keyPathsDepenses.add(
				DEPENSES_MONTANT_RESTE.key()
			); 

		contratDepenseControleFetchSpecification.setRawRowKeyPaths(keyPathsDepenses);
		
		NSArray<NSDictionary<String, Object>> donneesDepenses = contratDepenseControleFetchSpecification.fetchRawRows(edc());

		// On remet tout dans le même tableau
		
		NSArray<NSDictionary<String, Object>> donnees = new NSMutableArray<NSDictionary<String,Object>>();
		for(NSDictionary<String, Object> donneesEngagement : donneesEngagements) {
			NSMutableDictionary<String, Object> nouvellesDonneesEngagement = new NSMutableDictionary<String, Object>();
			nouvellesDonneesEngagement.put(RESTE_SIMPLE_CON_ORDRE.key(), donneesEngagement.get(RESTE_SIMPLE_CON_ORDRE.key()));
			nouvellesDonneesEngagement.put(RESTE_SIMPLE_MONTANT_RESTE.key(), donneesEngagement.get(ENGAGEMENTS_MONTANT_RESTE.key()));
			donnees.add(nouvellesDonneesEngagement);
		}
		
		for(NSDictionary<String, Object> donneesDepense : donneesDepenses) {
			NSMutableDictionary<String, Object> nouvellesDonneesDepense = new NSMutableDictionary<String, Object>();
			nouvellesDonneesDepense.put(RESTE_SIMPLE_CON_ORDRE.key(), donneesDepense.get(RESTE_SIMPLE_CON_ORDRE.key()));
			nouvellesDonneesDepense.put(RESTE_SIMPLE_MONTANT_RESTE.key(), donneesDepense.get(DEPENSES_MONTANT_RESTE.key()));
			donnees.add(nouvellesDonneesDepense);
		}
		
		donnees = ERXQ.filtered(donnees, RESTE_SIMPLE_MONTANT_RESTE.isNotNull());
		
		setDonneesResteEngageConventionsSimples(donnees);
		
	}
	
	
	public ERXKey<Integer> RESTE_RA_CON_ORDRE = new ERXKey<Integer>(Contrat.CON_ORDRE_KEY);
	
	public ERXKey<BigDecimal> RESTE_RA_MONTANT_RESTE = Contrat.TRANCHES
					.dot(Tranche.HISTO_CREDIT_POSITIONNES)
					.dot(HistoCreditPositionne.ENGAGEMENT_BUDGETS)
					.dot(EOEngagementBudget.ENG_MONTANT_BUDGETAIRE_RESTE);
	
	public ERXKey<String> RESTE_RA_HCP_SUPPR = Contrat.TRANCHES
			.dot(Tranche.HISTO_CREDIT_POSITIONNES)
			.dot(HistoCreditPositionne.HCP_SUPPR);
	
	public void initialiserDonneesResteEngageConventionsRa(NSArray<Contrat> conventions, EOExercice exercice) {
		NSArray<Contrat> conventionsRa = ERXQ.filtered(conventions, ERXQ.isTrue("isModeRA"));
		NSArray<Integer> conOrdres = (NSArray<Integer>) conventionsRa.valueForKey(Contrat.CON_ORDRE.key());
		
		EOQualifier qualifier = 
				Contrat.CON_ORDRE.in(conOrdres)
				.and(
					Contrat.TRANCHES
					.dot(Tranche.EXERCICE_COCKTAIL)
					.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice().intValue())
				)
				.and(Contrat.TRANCHES.dot(Tranche.TRA_SUPPR).eq(Tranche.TRA_SUPPR_NON));
		
		ERXFetchSpecification<Contrat> contratFetchSpecification = 
				new ERXFetchSpecification<Contrat>(
						Contrat.ENTITY_NAME, 
						qualifier,
						null);
		
		NSArray<String> keyPaths = new NSMutableArray<String>();
		
		keyPaths.add(RESTE_RA_CON_ORDRE.key());
		keyPaths.add(RESTE_RA_MONTANT_RESTE.key()); 
		keyPaths.add(RESTE_RA_HCP_SUPPR.key()); 

		contratFetchSpecification.setRawRowKeyPaths(keyPaths);
		
		NSArray<NSDictionary<String, Object>> conventionsFiltrees = contratFetchSpecification.fetchRawRows(edc());
		conventionsFiltrees = ERXQ.filtered(conventionsFiltrees, RESTE_RA_HCP_SUPPR.eq("N"));
		conventionsFiltrees = ERXQ.filtered(conventionsFiltrees, RESTE_RA_MONTANT_RESTE.isNotNull());

		setDonneesResteEngageConventionsRa(conventionsFiltrees);
		
	}
	
	
	public Boolean getTousLesEngagementsSontSoldes(Contrat convention) {

		BigDecimal total = BigDecimal.ZERO;
		
		if(convention.isModeRA()) {
			NSArray<NSDictionary<String, Object>> restesContrats = 
					ERXQ.filtered(getDonneesResteEngageConventionsRa(), RESTE_RA_CON_ORDRE.eq(convention.conOrdre()));
			total = (BigDecimal) restesContrats.valueForKey(ERXKey.sum(RESTE_RA_MONTANT_RESTE).key());
		} else {
			NSArray<NSDictionary<String, Object>> restesContrat = 
					ERXQ.filtered(getDonneesResteEngageConventionsSimples(), RESTE_SIMPLE_CON_ORDRE.eq(convention.conOrdre()));
			total = (BigDecimal) restesContrat.valueForKey(ERXKey.sum(RESTE_SIMPLE_MONTANT_RESTE).key());
		}
		
		return total.signum() == 0;	
		
	}
	
	public ERXKey<Integer> ETAT_SIMPLE_CON_ORDRE = new ERXKey<Integer>(Contrat.CON_ORDRE_KEY);
	public ERXKey<String> ETAT_SIMPLE_MAN_ETAT = Contrat.DEPENSE_CONTROLE_CONVENTIONS
			.dot(EODepenseControleConvention.DEPENSE_BUDGET)
	    	.dot(EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES)
	    	.dot(EODepenseControlePlanComptable.MANDAT)
	    	.dot(EOMandat.MAN_ETAT);
	
	public void initialiserDonneesEtatsMandatsConventionSimples(NSArray<Contrat> conventions, EOExercice exercice) {

		NSArray<Contrat> conventionsSimple = ERXQ.filtered(conventions, ERXQ.isFalse("isModeRA"));

		NSArray<Integer> conOrdres = (NSArray<Integer>) conventionsSimple.valueForKey(Contrat.CON_ORDRE.key());

		
		ERXFetchSpecification<Contrat> contratFetchSpecification = 
				new ERXFetchSpecification<Contrat>(
						Contrat.ENTITY_NAME, 
						Contrat.CON_ORDRE.in(conOrdres).and(Contrat.DEPENSE_CONTROLE_CONVENTIONS.dot(EODepenseControleConvention.EXERCICE).dot(EOExercice.EXE_EXERCICE_KEY).eq(exercice.exeExercice().intValue())),
						null);
		
		NSArray<String> keyPaths = new NSMutableArray<String>();
		
		keyPaths.add(ETAT_SIMPLE_CON_ORDRE.key());
		keyPaths.add(ETAT_SIMPLE_MAN_ETAT.key()); 
	
		contratFetchSpecification.setRawRowKeyPaths(keyPaths);
		setDonneesEtatsMandatsConventionsSimples(contratFetchSpecification.fetchRawRows(edc()));
		
	}
	
	
	public ERXKey<Integer> ETAT_RA_CON_ORDRE = new ERXKey<Integer>(Contrat.CON_ORDRE_KEY);
	
	public ERXKey<String> ETAT_RA_MAN_ETAT = Contrat.TRANCHES
					.dot(Tranche.HISTO_CREDIT_POSITIONNES)
					.dot(HistoCreditPositionne.ENGAGEMENT_BUDGETS)
					.dot(EOEngagementBudget.DEPENSE_BUDGETS)
			    	.dot(EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES)
			    	.dot(EODepenseControlePlanComptable.MANDAT)
			    	.dot(EOMandat.MAN_ETAT);
	
	public ERXKey<String> ETAT_RA_HCP_SUPPR = Contrat.TRANCHES
			.dot(Tranche.HISTO_CREDIT_POSITIONNES)
			.dot(HistoCreditPositionne.HCP_SUPPR);
	
	public void initialiserDonneesEtatsMandatsConventionsRa(NSArray<Contrat> conventions, EOExercice exercice) {
		NSArray<Contrat> conventionsRa = ERXQ.filtered(conventions, ERXQ.isTrue("isModeRA"));
		NSArray<Integer> conOrdres = (NSArray<Integer>) conventionsRa.valueForKey(Contrat.CON_ORDRE.key());
		
		EOQualifier qualifier = 
				Contrat.CON_ORDRE.in(conOrdres)
				.and(
					Contrat.TRANCHES
					.dot(Tranche.EXERCICE_COCKTAIL)
					.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice().intValue())
				)
				.and(Contrat.TRANCHES.dot(Tranche.TRA_SUPPR).eq(Tranche.TRA_SUPPR_NON));
		
		ERXFetchSpecification<Contrat> contratFetchSpecification = 
				new ERXFetchSpecification<Contrat>(
						Contrat.ENTITY_NAME, 
						qualifier,
						null);
		
		NSArray<String> keyPaths = new NSMutableArray<String>();
		
		keyPaths.add(ETAT_RA_CON_ORDRE.key());
		keyPaths.add(ETAT_RA_MAN_ETAT.key()); 
		keyPaths.add(ETAT_RA_HCP_SUPPR.key()); 

		contratFetchSpecification.setRawRowKeyPaths(keyPaths);
		
		NSArray<NSDictionary<String, Object>> conventionsFiltrees = contratFetchSpecification.fetchRawRows(edc());
		conventionsFiltrees = ERXQ.filtered(conventionsFiltrees, ETAT_RA_HCP_SUPPR.eq("N"));
		setDonneesEtatsMandatsConventionsRa(conventionsFiltrees);
		
	}
	
	public Boolean getTousLesMandatsSontVises(Contrat convention) {
		
		NSArray<String> manEtats = new NSArray<String>();
		
		if(convention.isModeRA()) {
			NSArray<NSDictionary<String, Object>> etatsContrats = 
					ERXQ.filtered(getDonneesEtatsMandatsConventionsRa(), ETAT_RA_CON_ORDRE.eq(convention.conOrdre()));
			manEtats = (NSArray<String>) etatsContrats.valueForKey(ETAT_RA_MAN_ETAT.key());
		} else {
			NSArray<NSDictionary<String, Object>> etatsContrats = 
					ERXQ.filtered(getDonneesEtatsMandatsConventionsSimples(), ETAT_SIMPLE_CON_ORDRE.eq(convention.conOrdre()));
			manEtats = (NSArray<String>) etatsContrats.valueForKey(ETAT_SIMPLE_MAN_ETAT.key());
		}
		
		manEtats = ERXArrayUtilities.arrayWithoutDuplicates(manEtats);
		manEtats = ERXArrayUtilities.removeNullValues(manEtats);

		manEtats.remove(EOMandat.ETAT_VISE);
		manEtats.remove(EOMandat.ETAT_PAYE);
		
		return manEtats.isEmpty();	

	}
	
	public Boolean getTousLesDepensesOntUnBordereau(Contrat convention) {
		
		NSArray<String> manEtats = new NSArray<String>();
		
		if(convention.isModeRA()) {
			NSArray<NSDictionary<String, Object>> etatsContrats = 
					ERXQ.filtered(getDonneesEtatsMandatsConventionsRa(), ETAT_RA_CON_ORDRE.eq(convention.conOrdre()));
			manEtats = (NSArray<String>) etatsContrats.valueForKey(ETAT_RA_MAN_ETAT.key());
		} else {
			NSArray<NSDictionary<String, Object>> etatsContrats = 
					ERXQ.filtered(getDonneesEtatsMandatsConventionsSimples(), ETAT_SIMPLE_CON_ORDRE.eq(convention.conOrdre()));
			manEtats = (NSArray<String>) etatsContrats.valueForKey(ETAT_SIMPLE_MAN_ETAT.key());
		}
		
		manEtats = ERXArrayUtilities.arrayWithoutDuplicates(manEtats);

		if(manEtats.contains(NSKeyValueCoding.NullValue)) {
			return false;
		}

		
		return true;	

	}
	
	
	public NSArray<NSDictionary<String, Object>> getDonneesMontantsDepensesConventionsSimples() {
		return donneesMontantsDepensesConventionsSimples;
	}

	public void setDonneesMontantsDepensesConventionsSimples(
			NSArray<NSDictionary<String, Object>> donneesMontantsDepensesConventionsSimples) {
		this.donneesMontantsDepensesConventionsSimples = donneesMontantsDepensesConventionsSimples;
	}

	public NSArray<NSDictionary<String, Object>> getDonneesMontantsDepensesConventionsRa() {
		return donneesMontantsDepensesConventionsRa;
	}

	public void setDonneesMontantsDepensesConventionsRa(
			NSArray<NSDictionary<String, Object>> donneesMontantsDepensesConventionsRa) {
		this.donneesMontantsDepensesConventionsRa = donneesMontantsDepensesConventionsRa;
	}

	public NSArray<NSDictionary<String, Object>> getDonneesResteEngageConventionsSimples() {
		return donneesResteEngageConventionsSimples;
	}

	public void setDonneesResteEngageConventionsSimples(
			NSArray<NSDictionary<String, Object>> donneesResteEngageConventionsSimples) {
		this.donneesResteEngageConventionsSimples = donneesResteEngageConventionsSimples;
	}

	public NSArray<NSDictionary<String, Object>> getDonneesResteEngageConventionsRa() {
		return donneesResteEngageConventionsRa;
	}

	public void setDonneesResteEngageConventionsRa(
			NSArray<NSDictionary<String, Object>> donneesResteEngageConventionsRa) {
		this.donneesResteEngageConventionsRa = donneesResteEngageConventionsRa;
	}

	public NSArray<NSDictionary<String, Object>> getDonneesEtatsMandatsConventionsSimples() {
		return donneesEtatsMandatsConventionsSimples;
	}

	public void setDonneesEtatsMandatsConventionsSimples(
			NSArray<NSDictionary<String, Object>> donneesEtatsMandatsConventionsSimples) {
		this.donneesEtatsMandatsConventionsSimples = donneesEtatsMandatsConventionsSimples;
	}

	public NSArray<NSDictionary<String, Object>> getDonneesEtatsMandatsConventionsRa() {
		return donneesEtatsMandatsConventionsRa;
	}

	public void setDonneesEtatsMandatsConventionsRa(
			NSArray<NSDictionary<String, Object>> donneesEtatsMandatsConventionsRa) {
		this.donneesEtatsMandatsConventionsRa = donneesEtatsMandatsConventionsRa;
	}
	
	
	
}