package org.cocktail.cocolight.serveur.components.assistants.modules;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.cocktail.cocowork.server.metier.convention.TrancheBudget;
import org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec;
import org.cocktail.cocowork.server.metier.convention.VCreditsPositionnes;
import org.cocktail.cocowork.server.metier.convention.VCreditsPositionnesRec;
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderBudgetMasqueCredit;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSValidation.ValidationException;

public class ConventionBudgetLightest extends ConventionBudgetLight {
    
    private NSDictionary dicoTranchesAnnuellesLight;
    
    public ConventionBudgetLightest(WOContext context) {
        super(context);
    }
    
    public NSDictionary dicoTranchesAnnuellesLight() {
        if (dicoTranchesAnnuellesLight == null) {
            NSData data = new NSData(application().resourceManager().bytesForResourceNamed("AvenantTranchesAnnuellesLight.plist", null, NSArray.EmptyArray));
            dicoTranchesAnnuellesLight = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
        }
        return dicoTranchesAnnuellesLight;
    }

    @Override
    public WOActionResults supprimerCreditDepense() {
        try {
            TrancheBudget.supprimerTrancheBudget(ecForBudget(), selectedTrancheBudget(), null);
            saveChanges("Suppression de ligne de crédit enregistrée avec succès");
            resetCachesAllDepenses();
        } catch (ValidationException e) {
            session().addSimpleErrorMessage("Cocolight", e.getMessage());
        }
        return null;
    }
 
//    @Override
//    public WOActionResults ajouterCreditDepense() {
//        try {
//            setNewTrancheBudget(TrancheBudget.creerTrancheBudget(selectedTranche(), 
//                                                                application().getDernierExerciceOuvertOuEnPreparation(), 
//                                                                applicationUser().getUtilisateur().utlOrdre()));
//            // On positionne le reste à positionner sur le montant par défaut
//            getNewTrancheBudget().setTbMontant(resteAPositionner());
//            resetNomenclatures();
//        }
//        catch (ValidationException e) {
//            session().addSimpleErrorMessage("Cocolight", e.getMessage());
//        }
//        return null;
//    }
    
    @Override
    public WOActionResults supprimerCreditRecette() {
        try {
            TrancheBudgetRec.supprimerTrancheBudgetRec(ecForBudget(), selectedTrancheBudgetRec(), null);
            saveChanges("Suppression de ligne de crédit enregistrée avec succès");
            resetCachesAllRecettes();
        }
        catch (ValidationException e) {
            session().addSimpleErrorMessage("Cocolight", e.getMessage());
        }
        return null;
    }
    
//    @Override
//    public WOActionResults ajouterCreditRecette() {
//        try {
//            setNewTrancheBudgetRec(TrancheBudgetRec.creerTrancheBudget(selectedTranche(), 
//                            application().getDernierExerciceOuvertOuEnPreparation(), 
//                            applicationUser().getUtilisateur().utlOrdre()));
//            // On positionne le reste à positionner sur le montant par défaut
//            getNewTrancheBudgetRec().setTbrMontant(resteAPositionnerRec());
//            resetNomenclatures();
//        }
//        catch (ValidationException e) {
//            session().addSimpleErrorMessage("Cocolight", e.getMessage());
//        }
//        return null;
//    }
//    
    @Override
    public WOActionResults modifierCreditDepense() {
      try {
        setIsAjoutCredits(false);
        setIsEditionCredits(true);

        TrancheBudget trancheBudget = dgCreditsDepensesRecap().selectedObject();
        setNewTrancheBudget(TrancheBudget.creerTrancheBudget(selectedTranche(), 
                                                                trancheBudget.exercice(), 
                                                                applicationUser().getUtilisateur().utlOrdre()));
        // On positionne le reste à positionner sur le montant par défaut
        getNewTrancheBudget().setTbMontant(trancheBudget.tbMontant());
        getNewTrancheBudget().setOrgan(trancheBudget.organ());
        getNewTrancheBudget().setTypeCredit(trancheBudget.typeCredit());
        resetNomenclatures();
      }
      catch (ValidationException e) {
        session().addSimpleErrorMessage("Cocolight", e.getMessage());
      }

      return doNothing();
    }
    
    @Override
    public BigDecimal getAncienMontantCreditDepenses() {
      return selectedTrancheBudget().tbMontant();
    }
    
    @Override
    public WOActionResults modifierCreditRecette() {
      try {
          setIsAjoutCredits(false);
          setIsEditionCredits(true);

          TrancheBudgetRec trancheBudget = dgCreditsRecettesRecap().selectedObject();

          setNewTrancheBudgetRec(TrancheBudgetRec.creerTrancheBudget(selectedTranche(), 
                                                              trancheBudget.exercice(),
                                                              applicationUser().getUtilisateur().utlOrdre()));
          // On positionne le reste à positionner sur le montant par défaut
          getNewTrancheBudgetRec().setTbrMontant(trancheBudget.tbrMontant());
          getNewTrancheBudgetRec().setOrgan(trancheBudget.organ());
          getNewTrancheBudgetRec().setTypeCredit(trancheBudget.typeCredit());
          resetNomenclatures();
      }
      catch (ValidationException e) {
          session().addSimpleErrorMessage("Cocolight", e.getMessage());
      }
      return null;
    }
    
    @Override
    public BigDecimal ancienMontantCreditRecettes() {
      return selectedTrancheBudgetRec().tbrMontant();
    }
    
    /**
     * {@inheritDoc}
     * Dans le cas de la convention simple, on prend le reste à positionner sur la tranche.
     */
    @Override
    public BigDecimal resteAPositionnerPourAjoutCreditDep() {
        return resteAPositionnerTranche();
    }
    
    /**
     * @return le reste à positionner sur la tranche
     */
    public BigDecimal resteAPositionnerTranche() {
        return selectedTranche().resteAPositionne();
    }
    
    /**
     * @return le reste à positionner sur la convention
     */
    public BigDecimal resteAPositionnerConvention() {
        return contrat().totalResteAPositionner();
    }
    
    @Override
    public BigDecimal newTrancheBudgetPct() {
        BigDecimal result = BigDecimal.ZERO;
        if (getNewTrancheBudget() != null) {
            BigDecimal montantTotal = selectedTranche().traMontant();
            if (montantTotal.compareTo(BigDecimal.ZERO) != 0) {
                result = getNewTrancheBudget().tbMontant().divide(montantTotal, 5, RoundingMode.HALF_UP);
                result = result.multiply(BigDecimal.valueOf(100));
            }
        }
        return result;
    }
    
    @Override
    public void setNewTrancheBudgetPct(BigDecimal pct) {
        BigDecimal montantAPositionne = selectedTranche().traMontant().multiply(pct).divide(BigDecimal.valueOf(100));
        getNewTrancheBudget().setTbMontant(montantAPositionne);
    }
    
    @Override
    public BigDecimal newTrancheBudgetRecPct() {
        BigDecimal result = BigDecimal.ZERO;
        if (getNewTrancheBudgetRec() != null) {
            BigDecimal montantTotal = selectedTranche().traMontantFraisInclus();
            if (montantTotal.compareTo(BigDecimal.ZERO) != 0) {
                result = getNewTrancheBudgetRec().tbrMontant().divide(montantTotal, 5, RoundingMode.HALF_UP);
                result = result.multiply(BigDecimal.valueOf(100));
            }
        }
        return result;
    }
    
    @Override
    public BigDecimal resteAPositionnerRec() {
        return selectedTranche().traMontantFraisInclus().subtract(selectedTranche().totalPositionneRec());
    }
    
    @Override
    public void setNewTrancheBudgetRecPct(BigDecimal pct) {
        BigDecimal montantAPositionne = selectedTranche().traMontant().multiply(pct).divide(BigDecimal.valueOf(100));
        getNewTrancheBudgetRec().setTbrMontant(montantAPositionne);
    }
    
    public NSArray<EOTypeCredit> getAllTypesCreditDepense() {
        if (typesCreditDepense == null) {
            typesCreditDepense = FinderBudgetMasqueCredit.findTypesCreditExecFromNature(ecForBudget(), getExercice(), null, EOTypeCredit.TCD_TYPE_DEPENSE);
        }
        return typesCreditDepense;
    }
    
    public NSArray<EOTypeCredit> getAllTypesCreditRecette() {
        if (typesCreditRecette == null) {
            typesCreditRecette = FinderBudgetMasqueCredit.findTypesCreditExecFromNature(ecForBudget(), getExercice(), null, EOTypeCredit.TCD_TYPE_RECETTE);
        }
        return typesCreditRecette;
    }
    
    @Override
	public Boolean peutAjouterCreditsPositionnesDepense() {
		return peutModifierCreditsPositionnes() && selectedTranche() != null && selectedTranche().reportNplus1().signum() == 0;
	}
	
    
    @Override
    public Boolean peutModifierCreditsPositionnesDepense() {
    	return peutModifierCreditsPositionnes() && dgCreditsDepensesRecap() != null && dgCreditsDepensesRecap().selectedObject() != null && selectedTranche() != null && selectedTranche().reportNplus1().signum() == 0;
    }
    
    @Override
    public Boolean peutSupprimerCreditsPositionnesDepense() {
    	return peutModifierCreditsPositionnes() && dgCreditsDepensesRecap() != null && dgCreditsDepensesRecap().selectedObject() != null && selectedTranche() != null && selectedTranche().reportNplus1().signum() == 0;
    }
    
    
    @Override
	public Boolean peutAjouterCreditsPositionnesRecette() {
		return peutModifierCreditsPositionnes();
	}
	
    
    @Override
    public Boolean peutModifierCreditsPositionnesRecette() {
    	return peutModifierCreditsPositionnes() && dgCreditsRecettesRecap() != null && dgCreditsRecettesRecap().selectedObject() != null;
    }
    
    @Override
    public Boolean peutSupprimerCreditsPositionnesRecette() {
    	return peutModifierCreditsPositionnes() && dgCreditsRecettesRecap() != null && dgCreditsRecettesRecap().selectedObject() != null;
    }
    
}