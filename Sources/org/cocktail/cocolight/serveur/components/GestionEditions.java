package org.cocktail.cocolight.serveur.components;

import org.cocktail.cocolight.serveur.util.EditionsEtatDepensesParNaturePourConventionRA;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderExercice;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.reporting.server.CktlAbstractReporter;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXS;
import er.extensions.formatters.ERXTimestampFormatter;
import er.extensions.foundation.ERXTimestampUtilities;

public class GestionEditions extends MyWOComponent {
  
    private ReporterAjaxProgress reporterProgress;
    private String reportFilename;
    private CktlAbstractReporter reporter;

  
    private NSMutableArray<EOExercice> exercices;
    private EOExercice currentExercice;
    private EOExercice selectedExercice;
    
    public GestionEditions(WOContext context) {
        super(context);
    }
    
    
    public ReporterAjaxProgress getReporterProgress() {
      return reporterProgress;
    }

    public void setReporterProgress(ReporterAjaxProgress reporterProgress) {
      this.reporterProgress = reporterProgress;
    }


    public String getReportFilename() {
      return reportFilename;
    }
 
    public void setReportFilename(String reportFilename) {
      this.reportFilename = reportFilename;
    }


    public CktlAbstractReporter getReporter() {
      return reporter;
    }

    public void setReporter(CktlAbstractReporter reporter) {
      this.reporter = reporter;
    }
    
    
    public WOActionResults editerEtatDepenses() {
      if(getSelectedExercice() == null) {
        session().addSimpleErrorMessage("Erreur", "Vous devez sélectionner un exercice");
        context().response().setStatus(500);
        return doNothing();
      }
      reporterProgress = new ReporterAjaxProgress(100);
      String timeStamp = ERXTimestampFormatter.dateFormatterForPattern("%Y_%m_%d_%H_%M_%S").format(new NSTimestamp());
      reportFilename = "Cocolight - EtatDepensesParNatureConventionsRE - " + timeStamp + ".xls";
      reporter = EditionsEtatDepensesParNaturePourConventionRA.editionConventionsGeneralites(session().applicationUser().getPersId(), getSelectedExercice().exeExercice().intValue(), reporterProgress);
      
      
      return doNothing();

    }
    
    
    
    
    public NSArray<EOExercice> getExercices() {
      if(exercices == null) {
        exercices = FinderExercice.getExercices(ERXEC.newEditingContext()).mutableClone();
        ERXS.sort(exercices, ERXS.descs(EOExercice.EXE_EXERCICE_KEY));
      }
      return exercices;
    }




    public EOExercice getCurrentExercice() {
      return currentExercice;
    }


    public void setCurrentExercice(EOExercice currentExercice) {
      this.currentExercice = currentExercice;
    }




    public EOExercice getSelectedExercice() {
      return selectedExercice;
    }


    public void setSelectedExercice(EOExercice selectedExercice) {
      this.selectedExercice = selectedExercice;
    }




    public static class ReporterAjaxProgress extends CktlAbstractReporterAjaxProgress implements org.cocktail.reporting.server.jrxml.IJrxmlReportListener {
      
      public ReporterAjaxProgress(int maximum) {
          super(maximum);
      }
        
    }






    
    
}