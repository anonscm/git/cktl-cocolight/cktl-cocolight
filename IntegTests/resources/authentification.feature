# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à l'application Cocolight
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
	
  Scénario: Authentification d'un utilisateur habilité
    Soit Alexia habilitée à utiliser Cocolight
    Lorsqu'elle se connecte avec le login alexia et le mot de passe "motDePasse"
    Alors elle se retrouve sur la page d'accueil et voit le texte "Liste des conventions consultables"
    