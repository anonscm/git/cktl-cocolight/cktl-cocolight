package org.cocktail.cocolight.serveur.bdd;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(features="classpath:authentification.feature", glue="org.cocktail.fwkcktlwebapp.common.test.integ")
public class AuthentificationTests {
    
}
